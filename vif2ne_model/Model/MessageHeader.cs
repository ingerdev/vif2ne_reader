﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace vif2ne_model.Model
{
    [ComplexType][Owned]
    public class MessageHeader
    {      
        [Column("From")]
        public string From { get; set; }
        [Column("To")]
        public string To { get; set; }
        [Column("Date")]
        public DateTime Date { get; set; }
        [Column("Topic")]
        public string Topic { get; set; }

        //rubricks
        //List<string>
        [Column("JsonizedThemeSections")]
        public string JsonizedThemeSections { get; set; }

    }
}
