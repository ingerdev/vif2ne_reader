﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace vif2ne_model.Model
{   
    public class Message
    {
        /*
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        */
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Vif2neID { get; set; }
        //From, to, subject etc
     
        //Absent at website.
        public bool Deleted { get; set; }

        //user red this Message.
        public bool Viewed { get; set; }
        public MessageHeader Header { get; set; }

        //Message text as list of specific text objects (links,plain texts, line breaks, Headered/striketrough text etc)      
        //List<TextObject>
        public string  JsonizedText { get; set; }

        //Single-level replies. Each reply may have its own replies.
        public virtual ICollection<Message> Replies { get; set; }
    }
    public static class JsonHelpers
    {
        public static List<TextObject> JsonToTextObjects(this Message message)
        {
            return new List<TextObject>();
        }

        public static string TextObjectsToJson(this Message message, List<TextObject> text)
        {
            return "";
        }

        public static List<string> JsonToThemeSections(this MessageHeader message)
        {
            return new List<string>();
        }

        public static string ThemeSectionsToJson(this MessageHeader message, List<string> text)
        {
            return "";
        }
    }
}
