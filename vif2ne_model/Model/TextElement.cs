﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vif2ne_model.Model
{
    public class TextObject
    {

    }
    public class TextElement:TextObject
    {
        public string Text { get; private set; }
        public TextElement(string text)
        {
            Text = text;
        }
        public override string ToString()
        {
            return Text;
        }
    }
    public class LinkElement : TextObject
    {
        public string Text { get; private set; }
        public Uri Uri { get; private set; }
        public LinkElement(string text,Uri link)
        {
            Text = text;
            Uri = link;
        }
        public override string ToString()
        {
            return Text;
        }
    }
    public class LineBreakElement : TextObject
    {
        public LineBreakElement()
        {            
        }
        public override string ToString()
        {
            return "\r\n";
        }
    }
    public class HeaderElement : TextObject
    {
        public string Text { get; private set; }
        public int HeaderLevel { get; private set; }
        public HeaderElement(string text, int headerLevel)
        {
            Text = text;
            HeaderLevel = headerLevel;
        }
        public override string ToString()
        {
            return Text;
        }
    }

    public class UnknownElement : TextObject
    {
        public string InnerText { get; private set; }
        public UnknownElement(string innerText)
        {
            InnerText = innerText;
        }
        public override string ToString()
        {
            return InnerText;
        }
    }
}
