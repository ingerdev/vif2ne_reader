﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace vif2ne_parser
{
 /*
    class MessageId
    {
        public MessageId(int id)
        {
            Id = id.ToString();
        }

        public MessageId(string id)
        {
            Id = id;
        }

        public readonly string Id;
    }
    class MessageTreeNode
    {
        public MessageTreeNode(MessageId id, MessageTreeNode parent_node)
        {
            Id = id;
            ParentNode = parent_node;
        }
        public readonly MessageId Id;
        public readonly MessageTreeNode ParentNode;

        // ordered list of message nodes (answers
        public Lazy<List<MessageTreeNode>> ChildNodes = new Lazy<List<MessageTreeNode>>();
    }

    class ForumThread
    {
        const string FORUM_BASE_URL = @"https://www.vif2ne.org/nvk/forum/0/co/";
        
        public ForumThread()
        {
            _root_message_id = new MessageId(-1);
            _root_node = new MessageTreeNode(_root_message_id, null);
        }
        

        public static ForumThread Create(HtmlNode thread_node)
        {
            //single <span> contain two things:
            //set of <a> things, one of them contain link to message    
            //then <div> which contain list of <ul> tags
            //list of <ul> each of them have <div> that replicate
            //<span structure>
            //
            //each <ul> is answer to message   

            //parse message part
            ForumMessage message = get_message_from_node(thread_node);
            List<ForumThread> sub_threads = get_subthreads_from_node(thread_node);

            return new ForumThread(message,sub_threads);
        }

        private ForumThread(ForumMessage message, List<ForumMessage> sub_threads)
        {
            _message = message;
            _sub_threads = sub_threads;
        }
        private static List<ForumMessage> get_messages_from_node(HtmlNode thread_node)
        {
           
            var first_limit_node = thread_node.SelectSingleNode(@"//a[2]");
            var current_node = first_limit_node;

            var urls = new List<String>();
            while (current_node != null)
            {              
                urls.Add(get_message_url_from_node(current_node));
                current_node = current_node.NextSibling;
            }
            //return texts;
            throw new NotImplementedException();
        }

        private static ForumMessage get_message_from_node(HtmlNode thread_node)
        {
            var message_url = get_message_url_from_node(thread_node.SelectSingleNode(@"//a[2]"));
            var message_node = get_message_node(message_url);



            return ForumMessage.Create(message_node);
        }

        private static String get_message_url_from_node(HtmlNode thread_node)
        {
            return String.Format("{0}{1}", FORUM_BASE_URL,
               thread_node.GetAttributeValue("href", ""));
        }

        private static HtmlNode get_message_node(string message_link)
        {
            // Create an HtmlDocument object from URL
            HtmlWeb htmlWeb = Vif2ne_HtmlWeb.CreateCyrillicHtmlWeb();
            HtmlDocument htmlDocument = htmlWeb.Load(message_link);

            //root node contain long list of <span> elements with single div delimiter
            //this delimiter split pinned(by admin) spans and user spans
            //spans are threads.
            return htmlDocument.DocumentNode.SelectSingleNode(@"//body");
        }

        private readonly ForumMessage _message;
        private readonly List<ForumThread> _sub_threads;

        //all forum messages
        private readonly Dictionary<MessageId, ForumMessage> _messages;
        private readonly MessageId _root_message_id;
        private readonly MessageTreeNode _root_node;
    }
    */
}
