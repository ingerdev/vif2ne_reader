﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using vif2ne_model.Model;

namespace vif2ne_parser
{
    public class TreeParser
    {
        const string FORUM_BASE_URL = @"https://www.vif2ne.org/nvk/forum/0/co/";

        private const string forum_link = @"https://www.vif2ne.org/nvk/forum/0/co/tree";
        private CancellationToken _ct;
        public TreeParser(CancellationToken ct)
        {
            _ct = ct;
        }

        public (List<(int parent_message_id, int message_id)> pinned_messages,
            List<(int parent_message_id, int message_id)> user_messages) load_and_parse_html()
        {
            var root_node = get_span_root_node();
            (var pinned_spans,var user_spans) = load_spans(root_node);
            var pinned_message_ids = SpanTreeElementParser.parse_spans_as_trees(pinned_spans,_ct);
            var user_message_ids = SpanTreeElementParser.parse_spans_as_trees(user_spans,_ct);
            return (pinned_message_ids, user_message_ids);
        }

        public async Task<Message> LoadMessageAsync(int message_id)
        {

            var html_node = await get_message_node_async(message_id_to_url(message_id));
            return get_message_from_node(html_node);

        }

        private static string message_id_to_url(int message_id)
        {
            return $"{FORUM_BASE_URL}/{message_id}.htm";
        }
     
        private void parse_spans(List<HtmlNode> spans)
        {
            //left pane
            List<Message> message_threads = new List<Message>();
            foreach (var span in spans)
            {
                message_threads.Add(get_message_from_node(span));
                if (_ct != null) _ct.ThrowIfCancellationRequested();
            }
            return;
        }

        private HtmlNode get_span_root_node()
        {
            // Create an HtmlDocument object from URL
            HtmlWeb htmlWeb = Vif2ne_HtmlWeb.CreateCyrillicHtmlWeb();
            HtmlDocument htmlDocument = htmlWeb.Load(forum_link);

            //root node contain long list of <span> elements with single div delimiter
            //this delimiter split pinned(by admin) spans and user spans
            //spans are threads.
            return htmlDocument.GetElementbyId("root");
        }
        private (List<HtmlNode>, List<HtmlNode>) load_spans(HtmlNode root_node)
        {
            var forum_pinned_spans_count = root_node.SelectNodes("div[@id='startboard']/preceding-sibling::span").Count;

            //I assume SelectNodes will keep same indexes and first forum_pinned_spans_count spans will be admin pinned spans
            var spans = root_node.SelectNodes("span");

            //split global threads span list to admin-pinned and user threads lists
            var pinned_spans = spans.Take(forum_pinned_spans_count).ToList();
            var user_spans = spans.Skip(forum_pinned_spans_count).ToList();
            return (pinned_spans, user_spans);
         
        }

        private static Message get_message_from_node(HtmlNode thread_node)
        {
            //var x = thread_node.SelectNodes(@"a[2]");
            var message_url = get_message_url_from_node(thread_node.SelectSingleNode(@"a[2]"));
            var message_node = get_message_node(message_url);


            return MessageParser.Create(message_node,message_url);
        }

        private static String get_message_url_from_node(HtmlNode thread_node)
        {
            return String.Format("{0}{1}", FORUM_BASE_URL,
               thread_node.GetAttributeValue("href", ""));
        }
        private static HtmlNode get_message_node(string message_link)
        {
            // Create an HtmlDocument object from URL
            HtmlWeb htmlWeb = Vif2ne_HtmlWeb.CreateCyrillicHtmlWeb();
            HtmlDocument htmlDocument = htmlWeb.Load(message_link);

            //root node contain long list of <span> elements with single div delimiter
            //this delimiter split pinned(by admin) spans and user spans
            //spans are threads.
            return htmlDocument.DocumentNode.SelectSingleNode(@"//body");
        }
        private static async Task<HtmlNode> get_message_node_async(string message_link)
        {
            // Create an HtmlDocument object from URL
            HtmlWeb htmlWeb = Vif2ne_HtmlWeb.CreateCyrillicHtmlWeb();
            HtmlDocument htmlDocument = await htmlWeb.LoadFromWebAsync(message_link);

            //root node contain long list of <span> elements with single div delimiter
            //this delimiter split pinned(by admin) spans and user spans
            //spans are threads.
            return htmlDocument.DocumentNode.SelectSingleNode(@"//body");
        }
    }
}




