﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using vif2ne_reader.Data;
using vif2ne_model.Model;

namespace vif2ne_parser
{
    public class WebParserService
    {
        private MessagesContext _ctx;
        public WebParserService(MessagesContext ctx)
        {
            _ctx = ctx;
        }        


        //load absent messages from the website and save it to database  
        public async Task<List<Message>> UpdateMessagesAsync(CancellationToken ct)
        {
            //load ids of all available messages
            (List<(int parent_message_id, int message_id)> pinned_messages,
            List<(int parent_message_id, int message_id)> user_messages) =  await LoadMessagesAsync(ct);

            List<int> webIDs = new List<int>();
            webIDs.AddRange(pinned_messages.Select(x => x.parent_message_id));
            webIDs.AddRange(pinned_messages.Select(x => x.message_id));
            //webIDs.AddRange(user_messages.Select(x => x.parent_message_id));
            //webIDs.AddRange(user_messages.Select(x => x.message_id));
            webIDs = webIDs.Distinct().ToList();
            webIDs.Remove(0);


            var localIDs = await _ctx.GetAllIDsAsync();
            var deletedIDs = localIDs.Except(webIDs).ToList();
            var absentIDs = webIDs.Except(localIDs);

            await _ctx.MarkDeletedAsync(deletedIDs,ct);

            List<Message> messages = await LoadMessageAsync(absentIDs,ct);
            

            return messages;
        }

        private async Task<List<Message>> LoadMessageAsync(IEnumerable<int> ids,CancellationToken ct)
        {
            var tasks =Partitioner.Create(ids)
                .GetPartitions(4)
                .Select(partition => Task.Run(async () =>
                 {
                     using (partition)
                     {
                         List<Message> messages = new List<Message>();
                         while(partition.MoveNext())
                         {
                             int message_id = partition.Current;

                             TreeParser parser = new TreeParser(ct);
                             var message = await parser.LoadMessageAsync(message_id);
                             messages.Add(message);
                             if (ct != null) ct.ThrowIfCancellationRequested();
                         }
                         return messages;
                     }
                 }));

            await Task.WhenAll(tasks);
            return tasks.SelectMany(t => t.Result).ToList();
        }

        private async Task<(List<(int parent_message_id, int message_id)> pinned_messages,
            List<(int parent_message_id, int message_id)> user_messages)> LoadMessagesAsync(CancellationToken ct)
        {
            return await Task.Run(() =>
            {
                TreeParser parser = new TreeParser(ct);
                return parser.load_and_parse_html();
            }, ct);
        }

    }
}
