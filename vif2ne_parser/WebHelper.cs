﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;

namespace vif2ne_parser
{
    public class Vif2ne_HtmlWeb
    {
        private static Encoding vif2ne_encoding;

        static Vif2ne_HtmlWeb()
        {
           
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            vif2ne_encoding = Encoding.GetEncoding(1251);
        }
        public static HtmlWeb CreateCyrillicHtmlWeb()
        {
            
            var html_web = new HtmlWeb();
            html_web.OverrideEncoding = vif2ne_encoding;

            return html_web;
        }
    }
    
}
