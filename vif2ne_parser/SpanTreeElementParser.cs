﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace vif2ne_parser
{
    public static class SpanTreeElementParser
    {
        public static List<(int parent_message_id, int message_id)> parse_spans_as_trees(List<HtmlNode> spans,CancellationToken ct)
        {
            //message id, child message id   
            var tree_messages = new List<(int parent_message_id, int message_id)>();
                   
            object results_lock = new object();         
            Parallel.ForEach(spans, new ParallelOptions { MaxDegreeOfParallelism = 8 }, span =>
            {
                if (ct != null) ct.ThrowIfCancellationRequested();
                var storage = new List<(int, int)>();
                parse_tree_element(span, 0, storage,ct);
                lock (results_lock)
                {
                    tree_messages.AddRange(storage);
                }
            });
            return tree_messages;
        }

        private static void parse_tree_element(HtmlNode tree_node, int parent, List<(int, int)> storage,CancellationToken ct)
        {
            if (ct != null) ct.ThrowIfCancellationRequested();
            //hap dont want /a or /div when filtering direct childs
            //instead, use a or div
            var message_id = tree_node.SelectSingleNode("a[2]").Attributes["href"].Value;
            //remove ".htm" and convert to int           
            var message_id_int = Int32.Parse(message_id.Substring(0,message_id.Length - 4));
            var uls = tree_node.SelectNodes(@"div/ul");
            if (parent != 0)
                storage.Add((parent, message_id_int));
            if (uls == null)
                return;
            foreach (var ul in uls)
                parse_tree_element(ul, message_id_int, storage,ct);          
        }

    }
}
