﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using vif2ne_reader.Data;
using vif2ne_model.Model;

namespace vif2ne_reader.Parser
{
    public class WebParserService
    {
        private MessagesContext _ctx;
        public WebParserService(MessagesContext ctx)
        {
            _ctx = ctx;
        }        

        //load absent messages from the website and save it to database  
        public async Task<List<Message>> UpdateMessagesAsync(CancellationToken ct)
        {
            //load ids of all available messages
            (List<(int parent_message_id, int message_id)> pinned_messages,
            List<(int parent_message_id, int message_id)> user_messages) =  await LoadMessagesIdAsync(ct).ConfigureAwait(false);
            var webIDs = CleanMessageIds(new List<(int parent_message_id, int message_id)>[] 
            { 
                pinned_messages,
#if !DEBUG
                user_messages,
#endif
            });
            
         
            //localIDs are ids from webIDs already present in local dbase           
            var localIDs = (await _ctx.FindExistedIDsAsync(webIDs).ConfigureAwait(false)).ToHashSet();
            var newIDs = webIDs.Except(localIDs).ToHashSet();
            var oldIDs = localIDs.Intersect(webIDs).ToHashSet();
          

            //for deteled we should invent smart strategy as only non-expired messages should be threaded as deleted
            // 
            var dict = ConvertMessagePairsToDict(new List<(int parent_message_id, int message_id)>[]
            {
                pinned_messages,
#if !DEBUG
                user_messages,
#endif
            });

            //load old messages from database
            var oldMessagesTask = _ctx.GetMessagesByIds(oldIDs);
            //load new messages from website
            var newMessagesTask = LoadMessagesAsync(newIDs, ct);
            await Task.WhenAll(oldMessagesTask, newMessagesTask).ConfigureAwait(false);

            var oldMessages = oldMessagesTask.Result;
            var newMessages = newMessagesTask.Result;
          
            var messages = new Dictionary<int,Message>();
            foreach(var msg in oldMessages.Union(newMessages))
            {
                messages[msg.Vif2neID] = msg;
            }            
            foreach(var parentId in dict.Keys)
            {
                var parent = messages[parentId];
                if (parent.Replies == null)
                    parent.Replies = new List<Message>();
                else
                    parent.Replies.Clear();
                foreach (var childId in dict[parentId])
                    parent.Replies.Add(messages[childId]);
            }

            await _ctx.AddRangeAsync(newMessages).ConfigureAwait(false);
            _ctx.UpdateRange(oldMessages);

            await _ctx.SaveChangesAsync().ConfigureAwait(false);

            return messages.Values.ToList();
        }
        //given lists of id tuples, remove dublicates and zeroes
        private static HashSet<int> CleanMessageIds(params List<(int parent_message_id,int message_id)>[] messageLists)
        {
            List<int> ids = new List<int>();
            foreach (var list in messageLists)
            {
                ids.AddRange(list.Select(x => x.parent_message_id));
                ids.AddRange(list.Select(x => x.message_id));                           
            }
            ids.Remove(0);            
            return ids.Distinct().ToHashSet();
        }

        private static Dictionary<int, List<int>> ConvertMessagePairsToDict(params List<(int parent_message_id, int message_id)>[] messageLists)
        {
            var dict = new Dictionary<int, List<int>>();
            foreach (var list in messageLists)
            {
                foreach((var parent_message_id, var message_id) in list)
                {
                    if (!dict.ContainsKey(parent_message_id))
                        dict[parent_message_id] = new List<int>();
                    dict[parent_message_id].Add(message_id);
                }
            }
            return dict;
        }


        private async Task<List<Message>> LoadMessagesAsync(IEnumerable<int> ids,CancellationToken ct)
        {
            var tasks =Partitioner.Create(ids)
                .GetPartitions(8)
                .Select(partition => Task.Run(async () =>
                 {
                     using (partition)
                     {
                         List<Message> messages = new List<Message>();
                         while(partition.MoveNext())
                         {
                             int message_id = partition.Current;

                             TreeParser parser = new TreeParser(ct);
                             var message = await parser.LoadMessageAsync(message_id).ConfigureAwait(false);
                             messages.Add(message);
                             if (ct != null) ct.ThrowIfCancellationRequested();
                         }
                         return messages;
                     }
                 })).ToArray();

            await Task.WhenAll(tasks).ConfigureAwait(false);
            var list = tasks.SelectMany(t => t.Result).ToList();
            return list;
        }

        private static async Task<(List<(int parent_message_id, int message_id)> pinned_messages,
            List<(int parent_message_id, int message_id)> user_messages)> LoadMessagesIdAsync(CancellationToken ct)
        {
            return await Task.Run(() =>
            {
                TreeParser parser = new TreeParser(ct);
                return parser.load_and_parse_html();
            }, ct).ConfigureAwait(false);
        }

    }
}
