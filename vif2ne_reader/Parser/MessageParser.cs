﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;
using vif2ne_model.Model;
namespace vif2ne_reader.Parser
{
    public static class MessageParser
    {

        public static Message Create(HtmlNode message_node,int message_id)
        {
            var message = new Message();
            message.Vif2neID = message_id;
            message.Header = MessageHeaderParser.parse_message_header(message_node);
            message.JsonizedText = message.TextObjectsToJson(parse_message_text(message_node));
            //message.Replies = parse_replies(message_node);
            return message;
        }
        /// <summary>
        /// <body>
        /// <table>Header (from, to, rubric)</table>
        /// <center>Subject</center>
        /// <.... message text... >
        /// <hr/>
        /// <ul>
        ///     <span/>
        ///     ... - thread replies links
        ///     <span/>
        /// </ul>
        /// </body>
        /// </summary>
        /// <param name="message_node"></param>
        /// <returns></returns>
        private static List<TextObject> parse_message_text(HtmlNode message_node)
        {
            //getting all nodes from end of center tag till next hr tag         
            var first_limit_node = message_node.SelectSingleNode(@"//center").NextSibling;
            var last_limit_node = message_node.SelectSingleNode(@"//body/hr[1]");
            var current_node = first_limit_node;

            var texts = new List<TextObject>();
            while (current_node != last_limit_node && current_node != null)
            {
                texts.Add(html_element_to_textobject(current_node));
                current_node = current_node.NextSibling;
            }
            return texts;
        }
        private static List<String> parse_replies(HtmlNode message_node)
        {
            var spans = message_node.SelectNodes(@"//span");
            return null;
        }
        private static TextObject html_element_to_textobject(HtmlNode node)
        {
            switch (node.Name)
            {
                case "BR": return new LineBreakElement();
                case "#text": return new TextElement(node.InnerText);
                default: return new UnknownElement(node.InnerText);
            }
        }
       
    }
}
