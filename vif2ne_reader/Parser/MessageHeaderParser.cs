﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using vif2ne_model.Model;

namespace vif2ne_reader.Parser
{
    public static class MessageHeaderParser
    {

        public static MessageHeader parse_message_header(HtmlNode message_node)
        {   //todo: rewrite to linq instead XPath
            MessageHeader header = new MessageHeader();
            var table_node = message_node.SelectSingleNode(@"//table");
            var topic_node = message_node.SelectSingleNode(@"//center");
            var tmp = table_node.SelectSingleNode(@"//tr[1]/td[3]/b");

            //dont use //tbody as it actually doesnt exists in the tags,
            //https://stackoverflow.com/questions/1678494/why-does-firebug-add-tbody-to-table

            var fr = table_node.SelectSingleNode(@"//tr[1]");
            var from = table_node.SelectSingleNode(@"//tr[1]/td[3]/b[1]")?.InnerText?.Trim('\r', '\n'); ;
            var to = table_node.SelectSingleNode(@"//tr[2]/td[2]/b[1]")?.InnerText?.Trim('\r', '\n');
            var date = table_node.SelectSingleNode(@"//tr[3]/td[2]/b[1]")?.InnerText?.Trim('\r', '\n');
            var topic = message_node.SelectSingleNode(@"//center/h3")?.InnerText?.Trim('\r', '\n');
            var themes = parse_themes(table_node);
            themes.Sort();

            //todo: parse rubricks
            header.From = from;
            header.To = to;
            header.Date = convert_to_datetime(date);            
            header.Topic = topic;
            header.JsonizedThemeSections = header.ThemeSectionsToJson(themes);

            return header;
        }
        private static DateTime convert_to_datetime(string datetime)
        {
            return DateTime.Parse(datetime);
        }
        private static List<String> parse_themes(HtmlNode table_node)
        {
            
            var joined_themess_string = table_node.SelectSingleNode(@"//tr[4]/td[2]/b[1]")?.InnerText?.Trim('\r', '\n');
            var themes = joined_themess_string.Split(";");
            return themes
                .Where(theme => !String.IsNullOrWhiteSpace(theme))
                .Select(theme => theme.Trim())
                .ToList();          
        }
    }
}
