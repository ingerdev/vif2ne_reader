﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using vif2ne_model.Model;

namespace vif2ne_reader.Data
{
    public class MessagesContext:DbContext
    {
        public DbSet<Message> Messages { get; set; }

        public Task<List<int>> FindExistedIDsAsync(HashSet<int> ids)
        {
            return Messages.Where(msg => ids.Contains(msg.Vif2neID) && !msg.Deleted).Select(msg => msg.Vif2neID).ToListAsync();
        }
        public Task<List<Message>> GetMessagesByIds(HashSet<int> ids)
        {
            return Messages.Where(msg => ids.Contains(msg.Vif2neID)).ToListAsync();
        }
        public Task<List<int>> GetAllIDsAsync(bool withDeleted = false)
        {
            return Messages.Where(msg => withDeleted || !msg.Deleted).Select(msg => msg.Vif2neID).ToListAsync();
        }

        public async Task<bool> MarkDeletedAsync(HashSet<int> listOfDeleted, CancellationToken ct)
        {
            var messagesToDelete = await Messages.Where(msg => listOfDeleted.Contains(msg.Vif2neID)).ToListAsync().ConfigureAwait(false);
            foreach (var message in messagesToDelete)
            {
                if (ct != null) ct.ThrowIfCancellationRequested();
                message.Deleted = true;
            }
            Messages.UpdateRange(messagesToDelete);
            return true;
        }

        public async Task<bool> AddNewEntities(List<Message> messages)
        {
            return true;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlite("Data Source = vif2ne.db; Integrated Security=True");
            //optionsBuilder.UseSqlite("Data Source = vif2ne.db");
            optionsBuilder.UseSqlite(@"Data Source=vif2ne.db");
        }
    }
}
