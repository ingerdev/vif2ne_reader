﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using vif2ne_model.Model;

namespace vif2ne_reader.ViewModels
{
    public class MessageViewModelIdEqualityComparer : IEqualityComparer<MessageViewModel>
    {
        public bool Equals([AllowNull] MessageViewModel x, [AllowNull] MessageViewModel y)
        {
            if (x == null && y == null)
                return true;
            else if (x == null || y == null)
                return false;

            return x.ID.Equals(y.ID);
        }

        public int GetHashCode([DisallowNull] MessageViewModel obj)
        {
            return obj.ID.GetHashCode();
        }
    }

    public sealed class MessageViewModelDateTimeComparer : IComparer<MessageViewModel>
    {
        public int Compare([AllowNull] MessageViewModel x, [AllowNull] MessageViewModel y)
        {
            if (x == null)
                return (y == null) ? 0 : 1;

            if (y == null)
                return -1;

            return x.Header.Date.CompareTo(y.Header.Date);

        }
    }
    public class MessageViewModelEqualityComparer : IEqualityComparer<MessageViewModel>
    {
        public bool Equals([AllowNull] MessageViewModel x, [AllowNull] MessageViewModel y)
        {
            if (x == null && y == null)
                return true;
            else if (x == null || y == null)
                return false;
            return (x.ID, x.Header, x.Text, x.Replies).Equals((y.ID, y.Header, y.Text, y.Replies));
        }

        public int GetHashCode([DisallowNull] MessageViewModel obj)
        {
            return (obj.ID, obj.Header, obj.Text, obj.Replies).GetHashCode();
        }
    }


    public class MessageViewModel : INotifyPropertyChanged
    {       
        public int ID { get; private set; }       
        public MessageHeader Header { get; private set; }

        //Message text as list of specific text objects (links,plain texts, line breaks, Headered/striketrough text etc)      
        public List<TextObject> Text { get; private set; }

        //Single-level replies. Each reply may have its own replies.
        private ObservableCollection<MessageViewModel> _replies;
        public ObservableCollection<MessageViewModel> Replies
        {
            get
            {
                return _replies;
            }
            set
            {
                _replies = value;
                NotifyPropertyChanged(nameof(Replies));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;      

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
