﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using vif2ne_reader.Data;
using vif2ne_reader.ViewModels;

namespace vif2ne_reader.Services
{
    public class MessageService
    {
        private static MessageViewModelIdEqualityComparer _equalityComparer;
        private static MessageViewModelDateTimeComparer _dateComparer;
        private MessagesContext _ctx;

        private static Dictionary<int, MessageViewModel> _oldMessagesDict, _newMessagesDict;
        static MessageService()
        {
            _oldMessagesDict = new Dictionary<int, MessageViewModel>();
            _newMessagesDict = new Dictionary<int, MessageViewModel>();
            _equalityComparer = new MessageViewModelIdEqualityComparer();
            _dateComparer = new MessageViewModelDateTimeComparer();
        }
        public MessageService(MessagesContext ctx)
        {
            _ctx = ctx;
        }
        public async Task UpdateMessagesVM([DisallowNull] ObservableCollection<MessageViewModel> messages, [AllowNull]CancellationToken ct)
        {            
                //delete nodes doesnt exist anymore
            var newMessages = await LoadMessages(ct).ConfigureAwait(false);
            UpdateMessages(newMessages, messages, ct);
           
        }
        public async Task<ObservableCollection<MessageViewModel>> LoadMessages([AllowNull]CancellationToken ct)
        {
            return null;
            /*
            return await Task.Run(async () =>
            {
               //_ctx.
                //await slu.DeleteGroupAsync(groupname);
            });
            */
        }    
        private void UpdateMessages(ObservableCollection<MessageViewModel> newMessages, ObservableCollection<MessageViewModel> messages,CancellationToken ct)
        {
            //if it isnt update but creation, assign collections.
            if (!messages.Any())
                messages = newMessages;
            else
            {
                //remove items didnt found in the new collection.
                foreach (var item in messages.Except(newMessages, _equalityComparer))
                {
                    if (ct != null) ct.ThrowIfCancellationRequested();
                    messages.Remove(item);
                }
                //add new messages
                foreach (var item in newMessages.Except(messages, _equalityComparer))
                {
                    if (ct != null) ct.ThrowIfCancellationRequested();
                    messages.Add(item);
                }
                messages.Sort(_dateComparer);

                //few runtime development checks
                if (messages.Count != newMessages.Count)
                    throw new Exception(@"Old/new messages count mismatch");
                for (int k = 0; k < messages.Count; ++k)
                {
                    if (messages[k].ID != newMessages[k].ID)
                        throw new Exception(@"Old/new message ID mismatch");
                }
            }
            //copy replies messages collections.
            for(int k = 0; k< messages.Count;++k)
                UpdateMessages(newMessages[k].Replies,messages[k].Replies,ct);
            
        }
        
    }
    static class ObservableCollectionHelper
    {
        //sort ObservableCollection inplace.
        public static void Sort<T>(this ObservableCollection<T> observable,IComparer<T> comparer)
        {
            List<T> sorted = observable.OrderBy(x => x).ToList();

            int ptr = 0;
            while (ptr < sorted.Count)
            {
                if (!observable[ptr].Equals(sorted[ptr]))
                {
                    T t = observable[ptr];
                    observable.RemoveAt(ptr);
                    observable.Insert(sorted.IndexOf(t), t);
                }
                else
                {
                    ptr++;
                }
            }
        }
    }
}
