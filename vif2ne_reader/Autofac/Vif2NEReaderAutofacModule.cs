﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using vif2ne_reader.Data;
using Module = Autofac.Module;

namespace vif2ne_reader.Autofac
{
    public class Vif2NEReaderAutofacModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<MessagesContext>().AsSelf();              

            var x = typeof(Vif2NEReaderAutofacModule).GetTypeInfo();
            _ = builder.RegisterAssemblyTypes(x.Assembly)
            .Where(t => t.Name.EndsWith("ViewModel",StringComparison.InvariantCulture));

        }
    }
}
