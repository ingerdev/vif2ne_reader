﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using vif2ne_model.Model;
using vif2ne_reader.Data;
using vif2ne_reader.Parser;

namespace vif2ne_reader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private async void StartButton_Click(object sender, RoutedEventArgs e)
        {
            //todo: async etc, disable button etc
            var cts = new CancellationTokenSource();
          
            List<Message> messages;
            using (var ctx = new MessagesContext())
            {
                ctx.Database.EnsureCreated();               
                var webParse = new WebParserService(ctx);
                messages = await webParse.UpdateMessagesAsync(cts.Token).ConfigureAwait(true);
            }
                        
            return;
        }

    }
}
