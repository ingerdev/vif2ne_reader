﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using vif2ne_reader.Autofac;

namespace vif2ne_reader
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            Module[] modules = new Module[] { };
            initAutofacModules(modules);
        }
        //creating service contained all dependency list
        //note: after creating kernel, no dependencies can be added
        public void initAutofacModules(IEnumerable<Module> additionalModules)
        {
            var modules = additionalModules.Concat(new Module[] { new Vif2NEReaderAutofacModule()});
            ContainerBuilder builder = new ContainerBuilder();
            foreach (var module in modules)
            {
                builder.RegisterModule(module);
            }
            builder.Build();

        }

    }
}
